#ifndef OMMAP_H
#define OMMAP_H
#include<map>
#include<vector>
#include<iostream>
#include<functional>
template<typename T, typename U>
class OrderedMMap
{
public:
    void insert(T key, U val)
    {
        map.push_back(std::make_pair(key, val));
    }
    U at(T key, size_t position) const
    {
        int idx = 0;
        for(auto it = map.begin();it!=map.end();++it)
        {
            if((*it).first==key) {
                if(idx++==position)
                {
                    return (*it).second;
                }
            }
        }
    }
    void erase(T key, size_t where)
    {
        int idx = 0;
        for(auto it = map.begin();it!=map.end();++it)
        {
            if((*it).first==key) {
                if(idx==where)
                {
                    map.erase(it);
                    return;
                }
                ++idx;
            }
        }
    }
    size_t size() const
    {
        return map.size();
    }
    size_t count(const T& key) const
    {
        int count = 0;
        for(auto it = map.begin();it!=map.end();++it)
        {
            if((*it).first==key) {
                    ++count;
            }
        }
        return count;
    }
    friend std::ostream & operator<< (std::ostream &out, const OrderedMMap &c)
    {
        for(auto it = c.map.begin();it!=c.map.end();++it)
        {
            out<<(*it).first<<':'<<(*it).second<< std::endl;;
        }
        return out;
    }
    U& operator ()(const T& par, size_t index) const
    {
        int idx = 0;
        for(auto it = map.begin();it!=map.end();++it)
        {
            if((*it).first==par) {
                if(idx==index)
                {
                    return const_cast<U&>((*it).second);
                }
                ++idx;
            }
        }
    }
    using iterator=typename std::vector<std::pair<T, U> >::const_iterator;
    iterator begin() { return map.cbegin(); }
    iterator end() { return map.cend(); }
private:
    std::vector<std::pair<T, U> > map;
};
#endif // OMMAP_H
