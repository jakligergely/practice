#ifndef ARRAY_H
#define ARRAY_H
#include <vector>


template<typename T, size_t U>
class Array
{
public:
    Array(const T& c)
    {
        vect.insert(vect.begin(), 2, c);
    }
    Array(const Array& from)
    {
        this->sizeOfVec = from.sizeOfVec;
        for (int i =0;i<from.size();++i) {
            this->vect.push_back(from.vect[i]);
        }
    }
    Array()
    {
        vect.reserve(U);
        this->sizeOfVec = U;
    }
    T at(size_t i) const
    {
        return vect[i];
    }
    size_t size() const
    {
        return sizeOfVec;
    }
    T& operator[](size_t i)
    {
        return vect[i];
    }
    const T& operator[](size_t i) const
    {
        return vect[i];
    }
    using iterator=typename std::vector<T >::const_iterator;
    iterator begin() const { return vect.begin(); }
    iterator end() const { return vect.end(); }
private:
    std::vector<T> vect;
    size_t sizeOfVec;
};

#endif // ARRAY_H
