#ifndef HASHMAP_H
#define HASHMAP_H
#include<vector>
template<typename T, typename U, typename V>
class HashMap
{
public:
    HashMap(const int& buckets) : buckets(buckets)
    {
        map = std::vector<std::pair<T, U>*>(buckets);
        func = new V(buckets);
    }
    void insert(T key, U val)
    {
        size_t index = (*func)(key);
        std::pair<T, U>* insert = new std::pair<T, U>();
        *insert = std::make_pair(key, val);
        map[index] = insert;
    }
    void erase(T key)
    {
        size_t index = (*func)(key);
        map[index] = NULL;
    }
    size_t size(unsigned long index) const
    {
        if(map[index])
        {
            return 1;
        }
        return 0;
    }
    size_t size() const
    {
        int retVal = 0;
        for(int i = 0; i<map.size();++i)
        {
            if(map[i])
                ++retVal;
        }
        return retVal;
    }
    U get(T key) const
    {
        return map[(*func)(key)]->second;
    }
    HashMap(const HashMap& par) :buckets(par.buckets)
    {
        this->func = par.func;
        this->map = par.map;
        for(int i = 0; i<par.map.size();++i)
        {
            if(par.map[i])
                this->map[i] = par.map[i];
        }
    }
private:
    V* func;
    std::vector<std::pair<T,U>* > map;
    const int buckets;
};
#endif // HASHMAP_H
