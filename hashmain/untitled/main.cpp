#include "hashtable.h"
#include <iostream>
using index_t = std::vector<int>::size_type;
template <class T>
class Func {
  const int n;
public:
  Func(int i): n(i) { }
  int operator()(const T& x) const { return x%n; }
};

template <class T>
class Func2 {
  const int n;
public:
  Func2(int i): n(i) { }
  int operator()(const T& x) const { return (x+1)%n; }
};

template<typename T, typename U >
class HashTable
{
public:
    HashTable(const unsigned int& bucketSize) : bucketSize(bucketSize)
    {
        fv = new U(static_cast<int>(bucketSize));
        vect.reserve(bucketSize);
    }
    void insert(T par)
    {
        if (vect[static_cast<index_t>((*fv)(par))].empty())
        {
            std::vector<int> tmp;
            vect[static_cast<index_t>((*fv)(par))] = tmp;
        }
        vect[static_cast<index_t>((*fv)(par))].push_back(par);
    }
    int size(T par) const
    {
        index_t index =static_cast<index_t>(par);
        return static_cast<int>(vect[index].size());
    }
    int size() const
    {
        int sum = 0;
        for (index_t i =0 ;i<bucketSize;++i) {
            if(!vect[i].empty())
            {
                sum += vect[i].size();
            }
        }
        return sum;
    }
    bool has(T par) const
    {
        if(std::find(vect[static_cast<index_t>((*fv)(par))].begin(), vect[static_cast<index_t>((*fv)(par))].end(), par) != vect[static_cast<index_t>((*fv)(par))].end())
        {
            return true;
        }
        return false;
    }
    void erase(T par)
    {
        for (index_t i =0 ;i<bucketSize;++i) {
            std::vector<T> newVec;
            if(!vect[i].empty())
            {
                for (index_t j=0;j<vect[i].size();++j) {
                    if(vect[i][j] == par)
                    {
                        continue;
                    }
                    newVec.push_back(vect[i][j] );
                }
            }
            vect[i] = newVec;
        }
    }
    HashTable(const HashTable& from) : bucketSize(from.bucketSize)
    {
        this->fv = from.fv;
        this->vect.reserve(bucketSize);
        for (unsigned int  i = 0;i<bucketSize;++i) {
            if(!from.vect[i].empty())
                this->vect[i] = from.vect[i];
        }
    }
private:
    const unsigned int bucketSize;
    U* fv;
    std::vector<std::vector<T> > vect;
};

int main() {
  int yourMark = 1;

  //2-es
  const unsigned int buckets = 47;
  HashTable<int, Func<int> > hti(buckets);
  hti.insert(82);
  hti.insert(59);
  hti.insert(12);
  const HashTable<int, Func<int> > chti=hti;
  yourMark = chti.size(12);

/* 3-as*/
  if (!chti.has(44) && hti.has(59))
    yourMark = chti.size();


  /* 4-es */
  hti.erase(59);
  yourMark+=hti.size(12);


  /* 5-os*/
  HashTable<int, Func2<int> > ht2(buckets);
  ht2.insert(12);
  if (ht2.size(12)==hti.size(12))
     std::cout << "Use another hashing function!" << std::endl;
  else
    ++yourMark;

  std::cout << "Your mark is " << yourMark << std::endl;

  return 0;
}
