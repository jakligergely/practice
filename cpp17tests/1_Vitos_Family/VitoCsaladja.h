//
// Created by revolutionist on 2018.01.07..
//

#ifndef CPP17TESTS_VITOCSALADJA_H
#define CPP17TESTS_VITOCSALADJA_H
#include <string>
#include <fstream>

class VitoCsaladja {
public:
    VitoCsaladja(const std::string filename);
    std::string computation();
private:
    std::ifstream inputFile;
    std::string fileName;
};


#endif //CPP17TESTS_VITOCSALADJA_H
