//
// Created by revolutionist on 2018.01.07..
//

#include "VitoCsaladja.h"
#include "gtest/gtest.h"

VitoCsaladja::VitoCsaladja(const std::string filename) {
    this->fileName = filename;

}
std::string VitoCsaladja::computation() {
    inputFile.open(fileName,std::ios_base::in);
    if (!inputFile.is_open()) {
        return "";
    }
    unsigned noOfCases;
    inputFile>>noOfCases;
    unsigned noOfDatas;
    unsigned distance;
    unsigned tmp;
    unsigned min, max;
    std::string retVal;
    for (int i = 0; i < noOfCases; ++i) {
        inputFile>>noOfDatas;
        if(!noOfDatas) { continue; }
        distance = 0;
        inputFile>>tmp;
        min = tmp;
        max = tmp;
        for (int j = 0; j < noOfDatas-1 ; ++j) {
            inputFile>>tmp;
            if(tmp<min)
            {
                min = tmp;
            }
            else if (tmp>max)
            {
                max = tmp;
            }
        }
        retVal += std::to_string(max-min);
        retVal += '\n';
    }
    inputFile.close();
    if(retVal.empty())
    {
        return "0";
    }
    else return retVal;
}
TEST(VitoCsaladja, esetek)
{
    testing::internal::CaptureStdout();
    VitoCsaladja input1("../1_Vitos_Family/test0.txt");
    std::cout<<input1.computation();
    GTEST_ASSERT_EQ(testing::internal::GetCapturedStdout(), "0");
}
TEST(VitoCsaladja, esetek2)
{
    testing::internal::CaptureStdout();
    VitoCsaladja input1("../1_Vitos_Family/test1.txt");
    std::cout<<input1.computation();
    GTEST_ASSERT_EQ(testing::internal::GetCapturedStdout(), "72\n30\n12\n");
}
TEST(VitoCsaladja, esetek3)
{
    VitoCsaladja input1("../1_Vitos_Family/test2.txt");
    GTEST_ASSERT_EQ(input1.computation(), "65\n72\n52\n74\n74\n64\n75\n74\n77\n40\n4\n");
}