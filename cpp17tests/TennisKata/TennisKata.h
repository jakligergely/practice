//
// Created by revolutionist on 2018.01.10..
//

#ifndef CPP17TESTS_TENNISKATA_H
#define CPP17TESTS_TENNISKATA_H
#include "gtest/gtest.h"
class TennisKata {
public:
    enum Players{PLAYER1, PLAYER2};
    void won(Players par)
    {
        if(par==player1&&!(player1>player2+2))
        {
            ++player1;
        }
        else if (par==player1&&!(player2>player1-2))
        {
            ++player2;
        }
    }
    std::pair<std::string, std::string> score() {
        std::string player1str, player2str;
        if(player1==0)
        {
            player1str="LOVE";
        }
        if(player1 == 1)
        {
            player1str ="FIFTEEN";
        }
        if(player1 == 2)
        {
            player1str ="THIRTY";
        }
        if(player1 == 3)
        {
            player1str ="FORTY";
        }
        if(player1>player2+1&&player2>3)
        {
            player2str ="ADVANTAGE";
        }
        if(player1>player2+2)
        {
            player1str ="WON";
        }
        if(player2==0)
        {
            player2str="LOVE";
        }
        if(player2 == 1)
        {
            player2str ="FIFTEEN";
        }
        if(player2 == 2)
        {
            player2str ="THIRTY";
        }
        if(player2 == 3)
        {
            player2str ="FORTY";
        }
        if(player2>player1+2)
        {
            player2str ="WON";
        }
        if(player2>player1+1&&player1>3)
        {
            player2str ="ADVANTAGE";
        }
        if(player1==player2&&player1>2)
        {
            player2str ="DEUCE";
            player1str ="DEUCE";
        }
        return std::make_pair(player1str, player2str);
    };
private:
    int player1 =0;
    int player2 =0;
};
TEST(TennisKata, esetek)
{
    GTEST_ASSERT_EQ(12, 12);
    TennisKata test;
    test.won(TennisKata::PLAYER1);
    std::cout<<test.score().first<<test.score().second;
   // test.won(TennisKata::Players::PLAYER1);
}
#endif //CPP17TESTS_TENNISKATA_H
