//
// Created by revolutionist on 2018.01.08..
//
#include <string>
#include "gtest/gtest.h"
const char STRIKE = 'X';
const char SPARE = '/';
const char MISS = '-';
unsigned score(const std::string& par)
{
    unsigned frameSUM= 0;
    for (unsigned i = 0; i < par.length() ; ++i) {
        switch (par[i])
        {
            case STRIKE:
                frameSUM+=10;
                if(i+2<par.length()-1)
                {
                    frameSUM+=score(par.substr(i+1, 2));
                }
                break;
            case MISS:
                break;
            case SPARE:
                frameSUM+=10-std::stoi(par.substr(i-1, 1));
                if(i+1<par.length()-1)
                {
                    frameSUM+=score(par.substr(i+1, 1));
                }
                break;
            default:
                frameSUM += std::stoi(par.substr(i, 1));
                break;
        }
    }
    return frameSUM;
}
TEST(BowlingGame, cases)
{
    GTEST_ASSERT_EQ(score("12345123451234512345"), 60);
    GTEST_ASSERT_EQ(score("XXXXXXXXXXXX"), 300);
    GTEST_ASSERT_EQ(score("9-9-9-9-9-9-9-9-9-9-"), 90);
    GTEST_ASSERT_EQ(score("5/5/5/5/5/5/5/5/5/5/5"), 150);
}